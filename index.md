% Alice & Kévin se marient !

:::: grid3

:::: {.text-side .blue}
# Alice et Kévin {.pretty}
# 29.02.2020
::::

:::: splash
![Faire part](images/faire-part.jpg "Venez nombreux !")
::::

:::: {.text-side .orange}
Venez compléter la Triforce !

[RSVP](rsvp.md){.button}

::::
::::

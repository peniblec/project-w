% Our story in a few words…

:::: mosaic

<!-- première ligne -->
:::: tile
![](../images/nous/Noel2012.jpg)
::::

:::: tile
![](../images/nous/Calin.jpg)
::::

:::: {.caption .purple}
Kévin meets Alice.  
He finds her cute.  
He wonders where her peculiar accent comes from.
::::

:::: tile
![](../images/nous/Bretagne.jpg)
::::

<!-- deuxième ligne -->
:::: tile
![](../images/nous/PraloEte.jpg)
::::

:::: {.caption .orange}
Alice goes to German classes, to a Ziket concert and to the student pub.  
There is this guy always wearing flip-flops and shorts.
::::

:::: tile
![](../images/nous/PraloHiverEsquimau.jpg)
::::

:::: {.caption .blue}
A fortunate random draw prevents Alice from playing tennis at university.  
She chooses rugby instead.
::::

<!-- troisième ligne -->
:::: tile
![](../images/nous/ChicagoKevin.jpg)
::::

:::: tile
![](../images/nous/Arboretum.jpg)
::::

:::: {.caption .orange}
A few training sessions later, their friendship is ruck-solid.
::::

::::: tile
![](../images/nous/Lumieres.jpg)
::::

<!-- quatrième ligne -->
:::: tile
![](../images/nous/ChicagoAlice.jpg)
::::

:::: tile
![](../images/nous/BigSable.jpg)
::::

:::: {.caption .purple}
Kévin discovers roasted chestnuts.  
Alice discovers Firefly.  
They kiss.
::::

:::: {.caption .blue}
They live together for 4 years.  
Then they swap parents.
::::

<!-- cinquième ligne -->
:::: tile
![](../images/nous/BretagneNez.jpg)
::::

:::: {.caption .orange}
Their love overcomes this trial and grows.  
They spend long evenings chatting and playing online.
::::

:::: tile
![](../images/nous/SaintMichel.jpg)
::::

:::: tile
![](../images/nous/PraloHiver.jpg)
::::

<!-- sixième ligne -->
:::: {.caption .blue}
Alice's sister talks about marriage.  
What a wonderful idea, how did they not think about it sooner!
::::

:::: tile
![](../images/nous/Gelato.jpg)
::::

:::: tile
![](../images/nous/ArboretumOies.jpg)
::::

:::: {.caption .purple}
They now know what they will spend their evenings on for the next 5 months…
::::

<!-- septième ligne -->
:::: tile
![](../images/nous/BretagneMer.jpg)
::::

:::: {.caption .purple}
For their love to slowly mature in the many years to come, they choose to celebrate it on February 29!
::::

:::: tile
![](../images/nous/Bisou.jpg)
::::

:::: tile
![](../images/nous/BigBen.jpg)
::::

::::

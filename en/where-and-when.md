% Useful information

:::: {.grid2 .purple}

:::: splash
![Carte](../images/carte.gif "It's dangerous to go alone! Take this! 💗")
::::

:::: text

# Where?
Domaine des Oliviers  
69620 Légny, in the Beaujolais province

# When?
29.02.2020  
Around 15h (12h30 if you want to help us prepare)

# What else?

## Dress code
Casual wear will contribute to the relaxed atmosphere  
The weather can be chilly in February though, clothes are recommended :p

## Since people have asked
Your presence will be the greatest gift  
People who wish to contribute to our honeymoon will find a box ready to hold your donations

::::
::::

:::: {.grid2 .orange}

:::: text

# Transportation

## By car
Highway A6 (exit 31.2) and D338  
There is ample parking space at the mansion

## Public transport
(direct train or train + coach depending on the schedule)  
The Bois d'Oingt Légny train station is 400 meters away from the reception hall and 40 minutes away from Lyon

::::
:::: text

# Accomodation

## Within walking distance
CITOTEL Côté Hôtel (2 mn away)  
Gîte des Vendanges - Delestra (12 mn away)

## Affordable
Maison Familiale et Rurale de Chessy: 18€ / person

<details>
<summary>Details
</summary>

- 6 minutes by car
- rooms with 2, 3 or 4 single beds
- bedsheets provided
- bring towels and pillowcases
- shared toilets

</details>

## More options
There are lots of B&Bs in the beaujolais area  
Find them on Google or on the [following list](../documents/liste_gites.pdf)

::::
::::

% We're getting married!

:::: grid3

:::: {.text-side .blue}
# Alice & Kévin {.pretty}
# 29.02.2020
::::

:::: splash
![Faire part](../images/faire-part.jpg "The more the merrier!")
::::

:::: {.text-side .orange}
Come complete the triforce!

[Confirm your presence](rsvp.md){.button}

::::
::::

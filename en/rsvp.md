% Send us an owl!

:::: grid3

:::: {.illustration-side .purple}
![](../images/Serenity.svg)
::::

:::: {.text .purple-on-orange}
## Please answer before 2020.01.15

Click on Hedwige to send your answer:

[![Hedwige](../images/Mariage_Chouette.png "🎵 Do you believe in maaa~gic 🎵")](https://forms.gle/uDwGVF5badPsbwDQ6)

She will bring us your message.

For our distant guests, we also hired interplanetary transports:

[![Serenity](../images/Mariage_Serenity.png "Ain't a thing in the verse can keep us apart.")](https://forms.gle/uDwGVF5badPsbwDQ6)

If you need more information, contact us at <contact@alice-et-kevin.com>.
::::

:::: {.illustration-side .blue}
![](../images/Hedwige.svg)
::::

::::

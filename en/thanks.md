% Quae sunt Caesaris Caesari.

Special thanks to [Soiz] for the wonderful [faire-part], for [Hedwige] and
[Serenity]!

Tools used to build this website:

- [tiled] and [Open Game Art] to design [the 16-bit map of France], [GIMP]
  to animate it.

- [Inkscape] for menu icons and the silhouettes on the [RSVP] page.

- [GitLab] for revision control and hosting.

- [NameSilo] for the domain name.

Sources of inspiration for the design:

- [Better Motherfucking Website].
- [Central Coffee] from [Coder Coder].

[Soiz]: http://soizic-legouguec.fr
[faire-part]: ../images/faire-part.jpg
[Hedwige]: ../images/Mariage_Chouette.png
[Serenity]: ../images/Mariage_Serenity.png

[tiled]: https://www.mapeditor.org/
[Open Game Art]: https://opengameart.org/
[the 16-bit map of France]: ../images/carte.gif
[GIMP]: https://www.gimp.org/
[Inkscape]: https://inkscape.org/
[RSVP]: rsvp.md

[GitLab]: https://gitlab.com/peniblec/project-w
[NameSilo]: https://namesilo.com

[Better Motherfucking Website]: http://bettermotherfuckingwebsite.com
[Central Coffee]: https://thecodercoder.github.io/central-coffee-demo/
[Coder Coder]: https://coder-coder.com/

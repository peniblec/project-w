% Nous, en quelques mots…

:::: mosaic

<!-- première ligne -->
:::: tile
![](images/nous/Noel2012.jpg)
::::

:::: tile
![](images/nous/Calin.jpg)
::::

:::: {.caption .purple}
Kévin rencontre Alice.  
Il la trouve mignonne.  
Il se demande d'où vient son accent étrange.
::::

:::: tile
![](images/nous/Bretagne.jpg)
::::

<!-- deuxième ligne -->
:::: tile
![](images/nous/PraloEte.jpg)
::::

:::: {.caption .orange}
Alice va en cours d'allemand, au concert Ziket, à la K-Fêt.  
Il y a un mec toujours en tongs et en bermuda.
::::

:::: tile
![](images/nous/PraloHiverEsquimau.jpg)
::::

:::: {.caption .blue}
Un tirage au sort heureux empêche Alice de faire du tennis à l'INSA.  
Elle choisit rugby à la place.
::::

<!-- troisième ligne -->
:::: tile
![](images/nous/ChicagoKevin.jpg)
::::

:::: tile
![](images/nous/Arboretum.jpg)
::::

:::: {.caption .orange}
Quelques entraînements plus tard, leur amitié est solide comme un ruck.
::::

::::: tile
![](images/nous/Lumieres.jpg)
::::

<!-- quatrième ligne -->
:::: tile
![](images/nous/ChicagoAlice.jpg)
::::

:::: tile
![](images/nous/BigSable.jpg)
::::

:::: {.caption .purple}
Kévin découvre les marrons grillés.  
Alice découvre Firefly.  
Ils s'embrassent.
::::

:::: {.caption .blue}
Ils vivent ensemble pendant 4 ans.  
Puis ils échangent leurs parents.
::::

<!-- cinquième ligne -->
:::: tile
![](images/nous/BretagneNez.jpg)
::::

:::: {.caption .orange}
Leur amour résiste à l'épreuve et grandit.  
Ils passent de longues soirées à causer et à jouer en ligne.
::::

:::: tile
![](images/nous/SaintMichel.jpg)
::::

:::: tile
![](images/nous/PraloHiver.jpg)
::::

<!-- sixième ligne -->
:::: {.caption .blue}
La sœur d'Alice parle de se marier.  
Quelle bonne idée, comment n'y ont-ils pas pensé plus tôt !
::::

:::: tile
![](images/nous/Gelato.jpg)
::::

:::: tile
![](images/nous/ArboretumOies.jpg)
::::

:::: {.caption .purple}
Ils savent maintenant ce qu'ils vont faire de leurs soirées pendant les 5 prochains mois…
::::

<!-- septième ligne -->
:::: tile
![](images/nous/BretagneMer.jpg)
::::

:::: {.caption .purple}
Pour que leur amour mature doucement dans les belles années à venir, ils choisissent de le célébrer le 29 février !
::::

:::: tile
![](images/nous/Bisou.jpg)
::::

:::: tile
![](images/nous/BigBen.jpg)
::::

::::

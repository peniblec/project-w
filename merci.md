% Quae sunt Caesaris Caesari.

Merci à [Soiz] pour le joli [faire-part], pour [Hedwige] et
[Serenity] !

Outils pour la réalisation de ce site :

- [tiled] et [Open Game Art] pour [la carte de France 16-bit], [GIMP]
  pour l'animation.

- [Inkscape] pour les icônes du menu et les silhouettes de la page
  [RSVP].

- [GitLab] pour la gestion des révisions et l'hébergement.

- [NameSilo] pour le nom de domaine.

- Méthode de développement certifiée à [![La RACHE]].

Inspirations pour l'apparence :

- [Better Motherfucking Website].
- [Central Coffee] de [Coder Coder].

[Soiz]: http://soizic-legouguec.fr
[faire-part]: images/faire-part.jpg
[Hedwige]: images/Mariage_Chouette.png
[Serenity]: images/Mariage_Serenity.png

[tiled]: https://www.mapeditor.org/
[Open Game Art]: https://opengameart.org/
[la carte de France 16-bit]: images/carte.gif
[GIMP]: https://www.gimp.org/
[Inkscape]: https://inkscape.org/
[RSVP]: rsvp.md

[GitLab]: https://gitlab.com/peniblec/project-w
[NameSilo]: https://namesilo.com
[![La RACHE]]: https://www.la-rache.com
[La RACHE]: https://www.la-rache.com/img/logo.png "La RACHE"

[Better Motherfucking Website]: http://bettermotherfuckingwebsite.com
[Central Coffee]: https://thecodercoder.github.io/central-coffee-demo/
[Coder Coder]: https://coder-coder.com/

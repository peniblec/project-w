# Website targets.

pages = $(patsubst ./%.md,public/%.html,$(shell find . -name '*.md'))
pages_folders = $(sort $(foreach p,$(pages),$(dir $(p))))
resources = public/images public/stylesheets public/documents

# Helpers.

template = helpers/template.html
# TODO: make CSS-inclusion less error-prone; move it to YAML metadata?
css = main.css menu.css
public/nous.html public/en/us.html: css += mosaic.css

# Recipes.

site: $(pages) $(resources)

# TODO: add dependency to menu.
$(pages): public/%.html: %.md $(template) | $(pages_folders)
	./helpers/page.sh $< $(css) > $@

$(resources): public/%: % | public
	cp -rT $< $@

public $(pages_folders):
	mkdir $@

clean:
	-rm -r public

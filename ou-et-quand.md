% Informations pratiques

:::: {.grid2 .purple}

:::: splash
![Carte](images/carte.gif "It's dangerous to go alone! Take this! 💗")
::::

:::: text

# Où ?
Domaine des Oliviers  
69620 Légny, dans le Beaujolais

# Quand ?
Le 29.02.2020  
À partir de 15h (12h30 si vous souhaitez participer aux préparatifs)

# Quoi d'autre ?

## Dress code
Nous comptons sur vous pour que l'ambiance soit décontractée  
Il risque de faire froid, habillez-vous quand même :p

## Puisqu'on nous l'a demandé
Votre présence sera le plus beau des cadeaux  
Ceux qui le souhaitent trouveront une urne pour participer à notre voyage de noces

::::
::::

:::: {.grid2 .orange}

:::: text

# Comment venir ?

## En voiture
Par l'A6 (sortie 31.2) et la D338  
Il y a un grand parking au domaine

## En transports
(direct ou train + car selon horaire)  
La gare de Bois d'Oingt Légny est à 400 mètres de la salle et 40 minutes de Lyon

::::
:::: text

# Où loger ?

## À côté
CITOTEL Côté Hôtel (2 mn à pieds)  
Gîte des Vendanges - Delestra (12 mn à pieds)

## Économique
Maison Familiale et Rurale de Chessy : 18€ / personne

<details>
<summary>Détails
</summary>

- à 6 minutes en voiture
- chambres de 2, 3 ou 4 lits simples
- draps compris
- serviettes et taies d'oreiller à apporter
- toilettes collectives

</details>

## Sinon
Gites et maisons d'hôtes en beaujolais  
À découvrir sur Google ou sur le [document ci-joint](documents/liste_gites.pdf)

::::
::::

#!/bin/bash

set -eu

md=$1
shift
css=("$@")

[[ ${md} =~ (([a-z]{2})/)?(.*\.md) ]]
lang=${BASH_REMATCH[2]:-fr}
page=${BASH_REMATCH[3]}

if test ${lang} = fr
then
    top_dir=.
else
    top_dir=..
fi
css_dir=${top_dir}/stylesheets
images_dir=${top_dir}/images

pandoc ${md}                                                    \
    -s                                                          \
    -f markdown-implicit_figures                                \
    $(for c in "${css[@]}" ; do echo -c ${css_dir}/${c} ; done) \
    -V images_dir=${images_dir}                                 \
    --template=helpers/template.html                            \
    --lua-filter=helpers/links-to-html.lua                      \
    -B <(./helpers/menu.sh ${page%.md} ${lang} < menu)

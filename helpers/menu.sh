#!/bin/bash

set -eu

# root menu format:
#     PAGE TAB LABEL TAB SYMBOL
# PAGE is the basename (without extension).
# LABEL is the text displayed on the page.
# SYMBOL is an alt emoji for the icon.

# foreign menu format:
#     ID TAB PAGE TAB LABEL
# ID is the equivalent page in the root menu.
# PAGE is the basename (without extension).
# LABEL is the text displayed on the page.

# The "main language" page basenames double as unique IDs to identify
# translations.

get-page-id ()
{
    local page=$1
    local lang=$2

    local id_regex="[^[:space:]]+"
    local sep=$'\t'
    local line_regex="^${id_regex}${sep}${page}${sep}.+"

    grep -E "${line_regex}" ${lang}/menu | cut -f1
}

get-foreign-page ()
{
    local id=$1
    local lang=$2

    local page_regex="[^[:space:]]+"
    local sep=$'\t'
    local line_regex="^${id}${sep}${page_regex}${sep}.+"

    grep -E "${line_regex}" ${lang}/menu | cut -f2
}

current_page=$1
current_lang=$2

top_dir=.
foreign_menu=
current_id=${current_page}
if test ${current_lang} != fr
then
    top_dir=..
    foreign_menu=${current_lang}/menu
    current_id=$(get-page-id ${current_page} ${current_lang})
fi

images_dir=${top_dir}/images

# Hamburger menu inspired by Erik Terwan - 2015 - MIT License.
# https://codepen.io/erikterwan/pen/EVzeRP

cat <<EOF
<nav>
  <div class="menu-toggle">
    <input type="checkbox" />

    <span class="burger-top"></span>
    <span class="burger-mid"></span>
    <span class="burger-bot"></span>

    <div class="menu-links">
EOF

while IFS=$'\t' read id label symbol
do
    class=''
    if test ${id} = ${current_id}
    then
        class=' class="current"'
    fi

    svg=${images_dir}/${id}.svg
    img="<img src=${svg} alt=\"${symbol}\" />"

    page=${id}
    if test ${foreign_menu}
    then
        IFS=$'\t' read id page label <<< $(grep "^${id}" ${foreign_menu})
    fi
    link=${page}.html

    echo "<p${class}><a href=\"${link}\">${img} ${label}</a></p>"
done

# TODO: deduce that from something instead of hardcoding it.  Exclude
# ${current_lang} so as to remove the if guard.
langs=(
    fr
    en
)

for lang in ${langs[@]}
do
    if test ${lang} = ${current_lang}
    then
        continue
    fi

    if test ${lang} = fr
    then
        link=${top_dir}/${current_id}.html
    else
        page=$(get-foreign-page ${current_id} ${lang})
        link=${top_dir}/${lang}/${page}.html
    fi

    svg=${images_dir}/${lang}.svg
    img="<img src=${svg} alt=\"💬\"/>"

    echo "<p><a href=\"${link}\">${img} ${lang^^}</a></p>"
done

cat <<EOF
    </div>
  </div>
</nav>
EOF
